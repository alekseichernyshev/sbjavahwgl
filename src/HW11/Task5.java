package HW11;

public class Task5 {

    public static boolean correctSequence(String string) {
        if (string.length() == 0){
            return true;
        }

        int count = 0;

        for (int i = 0; i< string.length(); i++) {

            if (string.charAt(i) == '(') {
                count ++;
            } else
                count --;
            if (count<0){
                return false;
            }
        }
        if (count == 0){
            return true;
        }else return false;

        }

    public static void main(String[] args) {
        System.out.println(correctSequence(")("));
    }

    }

