package HW11.Task2;

import HW11.Task1.IsLike;

@IsLike
public class Task2 {
    public static void main(String[] args) {
        inspectClass(Task2.class);
    }

    public static void inspectClass(Class<?> anyClass){
        System.out.println(anyClass.isAnnotationPresent(IsLike.class));
    }
}
