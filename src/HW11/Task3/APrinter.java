package HW11.Task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Objects;

public class APrinter {
    public void print(int a) {
        System.out.println(a);
    }

// throws InstantiationException, IllegalAccessException

    public static void main(String[] args) {
        APrinter aPrinter = new APrinter();
        Class<APrinter> clazz = APrinter.class;
        Method printMethod = null;
        try {
            printMethod = clazz.getMethod("print", int.class);
        } catch (NoSuchMethodException e){
            System.out.println("Такого метода в классе нет");
        }

       try {

           assert printMethod != null;
           printMethod.invoke(aPrinter, 5);
       }catch (InvocationTargetException e){
           System.out.println("Запрашиваемый метод вызывает исключение");
       }catch (IllegalAccessException e){
           System.out.println("Метод не имеет доступа к исполняемому классу");
       }catch (IllegalArgumentException e) {
           System.out.println("В метод передан не корректный аргумент");
       } catch (NullPointerException e) {
           System.out.println("Методу не передан аргумент");
       }


    }


}
