package HW11.Task4;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Task4 {

    public void getInterfaces(Object o) {

        Class<Object> clazz = Object.class;
        Class[] inter1 = clazz.getInterfaces();
        System.out.println(Arrays.toString(inter1)); // вывод на экран интерфейсов класса

        Set<Class> temp = new HashSet<>();
        if (clazz.getSuperclass() != null) {
            clazz = clazz.getSuperclass();
            temp.add(clazz);
        }

        Set<Class[]> temp2 = new HashSet<>();
        for (Class cl : temp) {
            temp2.add(cl.getInterfaces());
            System.out.println(Arrays.toString(cl.getInterfaces())); // вывод на экран интерфейсов родительских классов
        }

        for (Class cl : inter1) {
            System.out.println(Arrays.toString(cl.getInterfaces())); // вывод на экран интерфейсов наследуемых от интерфейсов класса
        }

        for (Class[] cl : temp2) {
            for (Class c : cl) {
                System.out.println(Arrays.toString(c.getInterfaces())); // вывод на экран интерфесов наследуемых от интерфейсов родительских классов
            }
        }

    }
}
