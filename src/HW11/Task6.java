package HW11;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Task6 {
    public static boolean correctSequence (String string) {
        if (string.length() == 0){
            return true;
        }

        Map<Character, Character> brackets = new HashMap<>();
        brackets.put(')', '(');
        brackets.put(']', '[');
        brackets.put('}', '{');

        Deque<Character> helper = new LinkedList<>();

        for (Character ch : string.toCharArray()) {
            if (brackets.containsValue(ch)){
                helper.push(ch);
            } else if (helper.isEmpty() || helper.pop() != brackets.get(ch)) {
                return false;

            }
        }
        return helper.isEmpty();
    }

    public static void main(String[] args) {
        System.out.println(correctSequence("{)(}"));
    }
}
