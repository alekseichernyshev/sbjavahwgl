package HW12;

import java.util.List;
import java.util.OptionalInt;

public class Task2 {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(1, 2, 3, 4, 5);
        Task2.getMultiplication(numbers);
    }

    public static void getMultiplication(List<Integer> numbers) {
        int result = numbers.stream()
                .mapToInt(a -> a)
                .reduce(1, (a, b) -> a * b);
        System.out.println(result);
    }
}
