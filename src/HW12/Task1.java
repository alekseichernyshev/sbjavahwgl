package HW12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Task1 {
    public static void main(String[] args) {

        Task1.getSum(100);

    }

    public static void getSum(int x) {

        List<Integer> num = Arrays.stream(IntStream.rangeClosed(1, x)
                        .toArray())
                .boxed()
                .toList();

        int sum = num.stream()
                .filter(n -> n % 2 == 0)
                .mapToInt(Integer::intValue)
                .sum();
        System.out.println(sum);

    }
}
