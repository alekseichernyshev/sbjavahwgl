package HW12;

import java.util.ArrayList;
import java.util.List;

public class Task5 {
    public static void main(String[] args) {
        List<String> task = List.of("abc", "def", "qqq");

        Task5.upperCase(task);
    }

    public static void upperCase(List<String> str) {
        List<String> temp = new ArrayList<>();
        try {
            temp = str.stream()
                    .map(String::toUpperCase)
                    .toList();
        } catch (RuntimeException ex){
            System.out.println(ex.getMessage());
        }

        for (int i = 0; i< temp.size(); i++) {
            if (i == temp.size()-1){
                System.out.println(temp.get(i));
            }else
                System.out.print(temp.get(i) + ", ");
        }

    }
}
