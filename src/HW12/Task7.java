package HW12;

import java.util.ArrayList;
import java.util.List;

public class Task7 {
    public static void main(String[] args) {
        String first = "caT";
        String second = "Cut";

        Task7.checkStrings(first, second);
    }

    public static void checkStrings(String str1, String str2) {

        if (str1.equalsIgnoreCase(str2)) {
            System.out.println(true);
        }
        if (str1.length() - str2.length() > 1 || str2.length() - str1.length() > 1) {
            System.out.println(false);
        }

        List<Character> list1 = Task7.stringToList(str1);
        List<Character> list2 = Task7.stringToList(str2);

        if (list1.size() < list2.size()) {
            list2.removeAll(list1);
            if (list2.size() > 1) {
                System.out.println(false);
            } else {
                System.out.println(true);
            }
        } else {
            list1.removeAll(list2);
            if (list1.size() > 1) {
                System.out.println(false);
            } else {
                System.out.println(true);
            }
        }
    }

    public static List<Character> stringToList(String str) {
        String str2 = str.toLowerCase();
        List<Character> list = new ArrayList<>();
        for (int i = 0; i < str.length(); i++) {
            list.add(str2.charAt(i));
        }

        return list;
    }

}
