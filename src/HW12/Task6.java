package HW12;

import java.util.*;
import java.util.stream.Collectors;

public class Task6 {

    public static Set<Integer> setsToSet(Set<Set<Integer>> set) {
        Set<Integer> result = new HashSet<>();
        try {
            result = set.stream()
                    .flatMap(Collection::stream)
                    .collect(Collectors.toSet());

        } catch (NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
}
