package HW12;

import java.util.List;

public class Task3 {
    public static void main(String[] args) {

        List<String> words = List.of("abc", "", "", "def", "qqq", "ppp");

        Task3.notEmpty(words);
    }

    public static void notEmpty(List<String> words) {

        int count =  words.stream()
                .filter(n -> n.length() > 0)
                .toList()
                .size();
        System.out.println(count);
    }
}
