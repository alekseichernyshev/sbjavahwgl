package HW12;

import java.util.Comparator;
import java.util.List;

public class Task4 {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(22, 33, 84562, 4847, 4, 54, 77, 554, 21);
        System.out.println(Task4.reverseSorted(numbers));
    }

    public static List<Integer> reverseSorted(List<Integer> numbers) {

        return numbers.stream()
                .sorted(Comparator.reverseOrder())
                .toList();
    }
}
