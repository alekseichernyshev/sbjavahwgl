package HW15;


// Напишите программу для проверки, является ли введение число - числом Армстронга
public class Armstrong {
    public static void main(String[] args) {
//        System.out.println("Please, insert number: ");
//        Scanner input = new Scanner(System.in);
//        int n = input.nextInt();
//        Armstrong a = new Armstrong();
//        System.out.println(a.checkNumber(n));
    }

    public boolean checkNumber(Integer n) {
        int sum = 0;
        int helper = n;
        int count = (int)Math.log10(n) + 1;
        while (helper != 0) {
            sum += Math.pow(helper % 10, count);
            helper /= 10;

        }
        return n == sum;
    }
}
