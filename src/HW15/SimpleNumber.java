package HW15;


import HW7.Book;

import java.math.BigInteger;

// Напишите программы, чтобы узнать, является ли введенное число простым или нет.
public class SimpleNumber {
    public static void main(String[] args) {
       SimpleNumber number = new SimpleNumber();
        System.out.println(number.checkSimpleNumber(30));
        System.out.println(number.checkSimpleNumber2(30));

    }
    // Методом перебора
    public boolean checkSimpleNumber(int num) {

        for (int i =2; i<num; i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
    // Встроенным методом
    public boolean checkSimpleNumber2(int num) {
        Integer n = num;
        BigInteger test = BigInteger.valueOf(n);
        return test.isProbablePrime((int) Math.log(n));
    }

}
