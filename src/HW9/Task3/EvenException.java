package HW9.Task3;

public class EvenException extends Exception{
    public EvenException() {
    }

    public EvenException(String message) {
        super(message);
    }
}
