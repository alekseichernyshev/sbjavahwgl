package HW9.Task3;

public class MyEvenNumber {
    int n;

    public int getN() {
        return n;
    }

    public void setN(int n)  {
        if (n%2!=0){
            try {
                throw new EvenException("Число не чётное");
            } catch (EvenException e) {
                throw new RuntimeException(e);
            }
        }
        this.n = n;
    }


}
