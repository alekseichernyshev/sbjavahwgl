package HW9.Task6and7;

public class Task7 {
    private Task7() {}

    // метод создания и заполнения массива используем из 6 задачи

    public static void findIndex(int p, int[] array) {
        int index = -1;
        for (int i = 0; i< array.length; i++) {
            if (array[i] == p){
                index = i;
                break;
            }
        }
        System.out.println(index);
    }
}
