package HW9.Task6and7;

import java.util.Arrays;
import java.util.Scanner;

public class Task6 {

    private Task6() {
    }

    public static int number() {
        int n = 0;
        Scanner console = new Scanner(System.in);
        try  {
             n = console.nextInt();
        } catch (Exception e){
            System.out.println("Это не целое число");
        }

        return n;
    }

    public static int[] createArray(int n) {
        System.out.println("Введите цифры");
        int[] array = new int[n];

        for (int i = 0; i<array.length; i++) {
            array[i] = Task6.number();
        }
        return array;
    }

    public static void findTwoMax(int[] array) {
        Arrays.sort(array);
        System.out.println(array[array.length-1] + ", " + array[array.length-2]);
    }
}
