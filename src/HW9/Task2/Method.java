package HW9.Task2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Method {
    public static void main(String[] args) {
        try(Scanner scanner = new Scanner(new File("test.txt"));
            PrintWriter writer = new PrintWriter(new File("testWrite.txt"))){
            while (scanner.hasNext()){
                writer.print(scanner.nextLine().toUpperCase());
            }
        }catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
    }
}
