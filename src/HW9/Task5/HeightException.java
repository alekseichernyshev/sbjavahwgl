package HW9.Task5;

public class HeightException extends Exception {

    public HeightException() {
    }

    public HeightException(String message) {
        super(message);
    }
}
