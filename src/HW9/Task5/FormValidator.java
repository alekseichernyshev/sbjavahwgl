package HW9.Task5;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormValidator {


    public static void checkName(String str) throws RuntimeException {
        char first = str.charAt(0);
        if (str.length() < 2 || str.length() > 20 || !(Character.isUpperCase(first))){
            throw new RuntimeException("Name is not correct");
        }

    }

    public static void checkBirthdate(String str) throws Exception {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");
        Date birthdate = format.parse(str);
        Date todaydate = new Date();
        String str2 = "01.01.1900";
        Date firstDate = format.parse(str2);
        if(birthdate.before(firstDate) || birthdate.after(todaydate)){
            throw new Exception("Дата не корректна");
        }
    }

    public static void checkGender(String str) {
        try {
            Gender.valueOf(str);
        } catch (IllegalArgumentException e){
            System.err.println(e.getMessage());
        }

    }

    public static void checkHeight(String str) throws HeightException {

        try {
            double height = Double.parseDouble(str);
        }catch (NumberFormatException e){
            throw new HeightException("Вы ввели не число");
        }
        if (Double.parseDouble(str)<=0){
            throw new HeightException("Не корректный рост");
        }
    }
}
