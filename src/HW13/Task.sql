create table flowers
(
    id             serial primary key,
    flower_name    varchar (30) not null,
    flower_price   int not null
);

create table clients
(
    id             serial primary key,
    clients_name   varchar(50) not null,
    phone_number   char(11) not null
);

create table orders
(
    id             serial primary key,
    deal_date      timestamp not null,
    buyer          int references clients ("id") not null,
    flower         int references flowers("id") not null,
    count          int check (count > 0 and count < 1001)
);
commit;

insert into flowers(flower_name, flower_price)
values ('Роза', 100)
insert into flowers(flower_name, flower_price)
values ('Лилия', 50)
insert into flowers(flower_name, flower_price)
values ('Ромашка', 25)

insert into clients(clients_name, phone_number)
values ('Иванов Иван Иванович', '89231431215')
insert into clients(clients_name, phone_number)
values ('Петров Пётр Петрович', '81235236688')

insert into orders(deal_date, buyer, flower, count)
values (now() - interval '5h', 1, 1, 5)
insert into orders(deal_date, buyer, flower, count)
values (now() - interval '24h', 2, 2, 25)
insert into orders(deal_date, buyer, flower, count)
values (now(), 1, 3, 105)

--1.	По идентификатору заказа получить данные заказа и данные клиента,  создавшего этот заказ

select
o.deal_date as "Дата и время покупки",
f.flower_name as "Наименование цветов",
f.flower_price as "Стоимость за единицу",
o.count as "Количество",
(f.flower_price * o.count) as "Итоговая стоимость",
c.clients_name as "ФИО покупателя",
c.phone_number as "Номер телефона покупателя"
from orders o
join clients c on c."id" = o.buyer
join  flowers f on f."id" = o.flower
where o.id = 3

-- 2.	Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
select
o.deal_date as "Дата и время покупки",
f.flower_name as "Наименование цветов",
f.flower_price as "Стоимость за единицу",
o.count as "Количество",
(f.flower_price * o.count) as "Итоговая стоимость",
c.clients_name as "ФИО покупателя",
c.phone_number as "Номер телефона покупателя"
from orders o
join clients c on c."id" = o.buyer
join  flowers f on f."id" = o.flower
where c.id = 1 and o.deal_date >=now() - interval '1 month'

--3.	Найти заказ с максимальным количеством купленных цветов, вывести их название и количество
select
f.flower_name as "Наименование цветов",
max(o.count) as "Количество"
from flowers f  join orders o
on f."id"=o.flower
group by f.flower_name
order by max(o."count") desc
limit 1



--4.	Вывести общую выручку (сумму золотых монет по всем заказам) за все время
select sum(f.flower_price * o.count) as "Сумма монет по всем заказам"
from flowers f, orders o
where f."id" = o."id"