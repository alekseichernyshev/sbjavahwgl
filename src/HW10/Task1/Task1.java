package HW10.Task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Task1 <T> {



    public  Set<T> unic (ArrayList<T> elements) throws Exception {

        if (elements.size() == 0) {
            throw new Exception("Список пуст");
        }
        return new HashSet<T>(elements);
    }
}
