package HW10.Task2;

import java.util.ArrayList;
import java.util.Collections;

public class Task2 {

    public boolean anagram (String str1, String str2) {

        if (str1.length() != str2.length()){
            return false;
        }

        ArrayList<Character> fistStr = new ArrayList<>();
        ArrayList<Character> secondStr = new ArrayList<>();

        for (char ch : str1.toCharArray()){
            fistStr.add(ch);
        }
        for (char ch : str2.toCharArray()){
            secondStr.add(ch);
        }

        Collections.sort(fistStr);
        Collections.sort(secondStr);

        return fistStr.equals(secondStr);
    }
}
