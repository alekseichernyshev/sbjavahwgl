package HW10.Task4;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Document {
    public int id;
    public String name;
    public int pageCount;



    public Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> organizeDocuments = new HashMap<>();
        Integer key = 1;
        for (Document doc : documents){
            organizeDocuments.put(key, doc);
            key++;
        }
        return organizeDocuments;
    }
}


