package HW10.Task3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class PowerfulSet {

    public <T> Set<T> intersection(Set<T> set1, Set<T> set2) throws Exception {
        if (set1.size() == 0 & set2.size() == 0) {
            throw new Exception("Оба списка пусты");
        }
        set1.retainAll(set2);
        return set1;
    }

    public <T> Set<T> union(Set<T> set1, Set<T> set2) throws Exception {
        if (set1.size() == 0 & set2.size() == 0) {
            throw new Exception("Оба списка пусты");
        }
        Set<T> result = new HashSet<>(set1);
        result.addAll(set2);

        return result;
    }

    public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) throws Exception {
        if (set1.size() == 0 & set2.size() == 0) {
            throw new Exception("Оба списка пусты");
        }
        set1.removeAll(set2);

        return set1;
    }
}
