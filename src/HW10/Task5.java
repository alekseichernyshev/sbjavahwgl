package HW10;

import java.util.*;
import java.util.stream.Collectors;

public class Task5 {
    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<>();
        words.add("the");
        words.add("day");
        words.add("is");
        words.add("sunny");
        words.add("the");
        words.add("the");
        words.add("the");
        words.add("sunny");
        words.add("is");
        words.add("is");
        words.add("day");

        Integer k = 4;

        System.out.println(result(words, k));

    }







    public static List<String> result(ArrayList<String> words, Integer k) {
        Map<String, Integer> temp = new HashMap<>();
        int count = 1;
        for (String value : words){
            if (temp.containsKey(value)){
                count = temp.get(value);
                count++;
            }
            temp.put(value, count);
        }
        Map<String, Integer> temp2 = temp.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(
                        Map.Entry :: getKey,
                        Map.Entry :: getValue,
                        (a, b) -> a,
                        LinkedHashMap::new
                ));
        LinkedList<String> temp3 = new LinkedList<>(temp2.keySet());
        System.out.println(temp3);
        List<String> result = new ArrayList<>();

       while (k>0){
           result.add(temp3.getLast());
           temp3.pollLast();
           k--;
       }

        return result;
    }


}
