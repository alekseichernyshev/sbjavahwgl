package HW7;

public class Visitor {
    private String name;
    private String id = null;
    private String bookTitle = null;

    public Visitor(String name, String id, String bookTitle) {
        this.name = name;
        this.id = id;
        this.bookTitle = bookTitle;
    }

    public Visitor(String name) {
        this.name = name;
    }

    public Visitor() {
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    @Override
    public String toString() {
        return "||" +
                "посетитель '" + name + '\'' +
                ", идентификатор '" + id + '\'' +
                ", название книги '" + bookTitle + '\'' +
                '}';
    }
}
