package HW7;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Library {

    ArrayList<Book> books = new ArrayList<>();

    public Library(ArrayList<Book> books) {
        this.books = books;
    }

    public Library() {}

    //1.	Добавить новую книгу в библиотеку, если книги с таким наименованием ещё нет в библиотеке.
    // Если книга в настоящий момент одолжена, то считается, что она всё равно есть в библиотеке (просто в настоящий момент недоступна).

    public List<Book> addNewBook(String title, String author) {

        if (books.contains(new Book(title))) {
            System.out.println("Такая книга уже есть в библиотеке");
        } else
            books.add(new Book(title, author));

        return books;
    }

    // 2.	Удалить книгу из библиотеки по названию, если такая книга в принципе есть в библиотеке и она в настоящий момент не одолжена.
    public List<Book> removeBook(String title) {

        for (int i = 0; i < books.size(); i++) {

            if (books.get(i).getTitle().equalsIgnoreCase(title) & books.get(i).getClient() == null) {
                books.remove(books.get(i));
                break;
            }
        }

        return books;
    }

    // 3.	Найти и вернуть книгу по названию.
    public Book findBookByTitle(String title) {
        Book book = null;
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getTitle().equalsIgnoreCase(title)) {
                book = books.get(i);
                break;
            }
        }
        return book;
    }

    // 4.	Найти и вернуть список книг по автору.
    public Book findBookByAuthor(String author) {
        Book book = null;
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getAuthor().equalsIgnoreCase(author)) {
                book = books.get(i);
                break;
            }
        }
        return book;
    }

    // 5.	Одолжить книгу посетителю по названию, если выполнены все условия:
    //a.	Она есть в библиотеке.
    //b.	У посетителя сейчас нет книги.
    //c.	Она не одолжена.
    //Также если посетитель в первый раз обращается за книгой — дополнительно выдать ему идентификатор читателя.

    public Visitor giveBook(String title, Visitor visitor){

        int idDigit = new Random().nextInt(100) * 15;

        Book book = findBookByTitle(title);

        if (book.getClient() == null & visitor.getBookTitle() == null){
            book.setClient(visitor.getName());
            visitor.setBookTitle(title);
        }else {
            System.out.println();
        }

        if (visitor.getId() == null){
            visitor.setId(visitor.getName() + idDigit);
        }

        return visitor;
    }



    // 6.	Вернуть книгу в библиотеку от посетителя, который ранее одалживал книгу.
    // Не принимать книгу от другого посетителя.
    // a.	Книга перестает считаться одолженной.
    // b.	У посетителя не остается книги.

    public Visitor pickUpBook(String title, Visitor visitor, Integer score){

        Book book = findBookByTitle(title);
        ArrayList<Integer> scores = new ArrayList<>();
        if (book.getClient().equals(visitor.getName())){
            book.setClient(null);
            visitor.setBookTitle(null);
            book.setScores(score);
        }

        return visitor;
    }

    // 8.	Добавить функционал оценивания книг посетителем при возвращении в библиотеку.
    // Оценка книги рассчитывается как среднее арифметическое оценок всех посетителей, кто брал эту книгу.
    // Реализовать метод, возвращающий оценку книги по её наименованию.
    public String bookRating(String title){

        Book book = findBookByTitle(title);
        int[] temp = book.getScores().stream().
                mapToInt(Integer::intValue)
                .toArray();
        int tempSum = 0;
        for (int i = 0; i<temp.length; i++){
            tempSum += temp[i];
        }
        double rating = (double) tempSum / temp.length;


        return String.format("%.2f", rating);
    }

    public void print(int[] array){
        for (int i = 0; i<array.length; i++){
            System.out.print(array[i] + " ");
        }
    }

}
