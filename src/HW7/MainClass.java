package HW7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainClass {
    public static void main(String[] args) {

        ArrayList<Book> books = new ArrayList<>();

        books.add(new Book("Собачье сердце", "Михаил Булгаков"));
        books.add(new Book("Отверженные", "Виктор Гюго"));
        books.add(new Book("Война и мир", "Лев Толстой"));
        books.add(new Book("Отцы и дети", "Иван Тургенев"));
        books.add(new Book("Белый клык", "Джек Лондон"));
        books.add(new Book("Американская трагедия", "Теодор Драйзер"));
        books.add(new Book("Гамлет", "Уильям Шекспир"));

        Library library = new Library(books);

        //1.	Добавить новую книгу в библиотеку, если книги с таким наименованием ещё нет в библиотеке.
        // Если книга в настоящий момент одолжена, то считается,
        // что она всё равно есть в библиотеке (просто в настоящий момент недоступна).

        // Добавляем новую книгу
        library.addNewBook("Мёртвые души", "Николай Гоголь");
        // Добавляем книгу с названием, которое уже есть
        library.addNewBook("Отверженные", "Уильем Николсон");
        // Добавляем книгу с автором который уже есть
        library.addNewBook("Мастер и Маргарита", "Михаил Булгаков");

        System.out.println(books);

        // 2.	Удалить книгу из библиотеки по названию,
        // если такая книга в принципе есть в библиотеке и она в настоящий момент не одолжена.
        library.removeBook("Гамлет");
        System.out.println("Удалили книгу");
        System.out.println(books);

        // 3.	Найти и вернуть книгу по названию.
        System.out.println("Ищем книгу по названию");
        System.out.println(library.findBookByTitle("Американская трагедия"));

        // 4.	Найти и вернуть список книг по автору.
        System.out.println("Ищем книгу по автору");
        System.out.println(library.findBookByAuthor("Джек Лондон"));

        //5.	Одолжить книгу посетителю по названию, если выполнены все условия:
        //a.	Она есть в библиотеке.
        //b.	У посетителя сейчас нет книги.
        //c.	Она не одолжена.
        //Также если посетитель в первый раз обращается за книгой — дополнительно выдать ему идентификатор читателя.
        Visitor visitor1 = new Visitor("Иван Иванович");
        Visitor visitor2 = new Visitor("Пётр Петрович");
        Visitor visitor3 = new Visitor("Фёдор Фёдорович");
        // Выдаём книгу
        library.giveBook("Отверженные", visitor1);
        System.out.println(visitor1);
        // Пробуем выдать эту же книгу другому читателю
        System.out.println();
        library.giveBook("Отверженные", visitor2);
        System.out.println(visitor2);
        // Читатель с книгой пытается взять ещё одну
        System.out.println();
        library.giveBook("Мастер и Маргарита", visitor1);
        System.out.println(visitor1);
        library.giveBook("Белый клык", visitor3);
        System.out.println(visitor3);

        // 6.	Вернуть книгу в библиотеку от посетителя, который ранее одалживал книгу.
        // Не принимать книгу от другого посетителя.
        //a.	Книга перестает считаться одолженной.
        //b.	У посетителя не остается книги.

        // Пытаемся сдать книгу которую взял другой читатель
        library.pickUpBook("Белый клык", visitor1, 5);
        System.out.println();
        System.out.println(visitor1);
        System.out.println(visitor3);

        // Сдаём книги, проставляем оценки
        library.pickUpBook("Белый клык", visitor3, 5);
        library.pickUpBook("Отверженные", visitor1, 5);

        library.giveBook("Отверженные", visitor2);
        library.pickUpBook("Отверженные", visitor2, 4);
        library.giveBook("Отверженные", visitor3);
        library.pickUpBook("Отверженные", visitor3, 5);

        //8.	Добавить функционал оценивания книг посетителем при возвращении в библиотеку.
        // Оценка книги рассчитывается как среднее арифметическое оценок всех посетителей, кто брал эту книгу.
        // Реализовать метод, возвращающий оценку книги по её наименованию.
        System.out.println("Рейтинг книги Отверженные " + (library.bookRating("Отверженные")));
        System.out.println("Рейтинг книги Белый клык " + (library.bookRating("Белый клык")));














    }
}
