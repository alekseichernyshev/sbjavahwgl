package HW7;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Book {
    private String title;
    private String author;
    private String client = null;
    private ArrayList<Integer> scores = new ArrayList<>();

    public ArrayList<Integer> getScores() {
        return scores;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getClient() {return client;}



    public void setTitle(String title) {this.title = title;}

    public void setAuthor(String author) {this.author = author;}

    public void setClient(String client) {this.client = client;}

    public void setScores(Integer score) {
        scores.add(score);
    }

    public Book(String title, String author) {
        this.title = title;
        this.author = author;
    }

    public Book(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(title, book.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author);
    }

    @Override
    public String toString() {
        return "|| " +
                "название " + title +
                ", автор " + author +
                ", client " + client + " ||" + '\n';
    }



}
