package HW5;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        int result = Recoursion(n);
        System.out.println(result);
    }

    public static int Recoursion(int n){
        int result = 0;
        if(n == 0){
            return 0;
        }else {
            result = n%10 + Recoursion(n/10);
        }
        return result;
    }
}
