package HW5;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();

        int[][] array = new int[n][n];
        int a = console.nextInt();
        int b = console.nextInt();
        int c = console.nextInt();
        int d = console.nextInt();

        for (int i = b; i<=d; i++){
            for (int j = a; j<=c; j++){
                array[i][j] = 1;
            }
        }
        array[3][2] = 0;


        for (int i = 0; i<n; i++){
            for (int j = 0; j<n; j++){
                if(j == n-1) {
                    System.out.print(array[i][j]);
                }else {
                    System.out.print(array[i][j] + " ");
                }
            }
            System.out.println();
        }
    }
}
