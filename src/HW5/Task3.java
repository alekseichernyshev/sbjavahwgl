package HW5;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();

        char[][] array = new char[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array[i][j] = '0';
            }
        }
        int a = console.nextInt();
        int b = console.nextInt();
        array[b][a] = 'K';

        if (b - 2 > 0 && a + 1 < n) {
            array[b - 2][a + 1] = 'X';
        }
        if (b - 2 > 0 && a - 1 > 0) {
            array[b - 2][a - 1] = 'X';
        }
        if (a + 2 < n && b + 1 < n) {
            array[b + 1][a + 2] = 'X';
        }
        if (a + 2 < n && b - 1 > 0) {
            array[b - 1][a + 2] = 'X';
        }
        if (b + 2 < n && a + 1 < n) {
            array[b + 2][a + 1] = 'X';
        }
        if (b + 2 < n && a - 1 > 0) {
            array[b + 2][a - 1] = 'X';
        }
        if (a - 2 > 0 && b + 1 < n) {
            array[b + 1][a - 2] = 'X';
        }
        if (a - 2 > 0 && b - 1 > 0) {
            array[b - 1][a - 2] = 'X';
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j == n - 1) {
                    System.out.print(array[i][j]);
                } else {
                    System.out.print(array[i][j] + " ");
                }
            }
            System.out.println();
        }
    }
}

