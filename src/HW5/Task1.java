package HW5;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        int m = console.nextInt();
        int[][] arr = new int[m][n];
        for (int i = 0; i<m; i++){
            for (int j = 0; j<n; j++){
                arr[i][j] = console.nextInt();
            }
        }

        for (int i= 0;i<m; i++){
            int min = arr[i][0];
            for (int j = 1; j<n; j++){
                if(arr[i][j]<min){
                    min = arr[i][j];
                }
            }
            System.out.print(min + " ");
        }
    }




}
