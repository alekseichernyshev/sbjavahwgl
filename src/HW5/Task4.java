package HW5;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        int n = console.nextInt();
        int[][] array = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array[i][j] = console.nextInt();
            }
        }
        int p = console.nextInt();
        int x = 0;
        int y = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (array[i][j] == p) {
                    x = i;
                    y = j;
                }
            }
        }
        int[][] resultArray = new int[n-1][n-1];
        int ind1, ind2;
        int i, j;
        for (i = 0, ind1 = 0; i<n; i++, ind1++) {
            if (i == x ) {
                ind1--;
                continue;
            }
            for (j = 0, ind2 = 0; j<array[i].length; j++, ind2++) {

                if (j == y) {
                    ind2--;
                    continue;
                }
                resultArray[ind1][ind2] = array[i][j];
            }
        }

        for ( i = 0; i< resultArray.length; i++){
            for (j = 0; j<resultArray[i].length; j++){
                if(j == resultArray[i].length-1) {
                    System.out.print(resultArray[i][j]);
                }else {
                    System.out.print(resultArray[i][j] + " ");
                }
            }
            System.out.println();
        }

    }
}
