package HW5;

import java.util.Arrays;
import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        String[] people = new String[n];
        for (int i = 0; i<n; i++){
            people[i] = console.next();
        }
        String[] dog = new String[n];
        for (int i = 0; i<n; i++){
            dog[i] = console.next();
        }

        int[][] scores = new int[n][3];
        for (int i = 0; i<n; i++){
            for (int j = 0; j< 3; j++){
                scores[i][j] = console.nextInt();
            }
        }

        double a = 0;
        double b = 0;
        double c = 0;
        double d = 0;
        for (int j = 0; j< 3; j++){
            a += scores[0][j];
            b += scores[1][j];
            c += scores[2][j];
            d += scores[3][j];
        }
        double scale = Math.pow(10,1);
        double score1 = Math.floor((a/3)*scale)/scale;
        double score2 = Math.floor((b/3)*scale)/scale;
        double score3 = Math.floor((c/3)*scale)/scale;
        double score4 = Math.floor((d/3)*scale)/scale;
        double[] resultArray = {score1, score2, score3, score4};
        Arrays.sort(resultArray);
        if(resultArray[resultArray.length-1] == score1){
            System.out.println(people[0] + ": " + dog[0] + ", " + score1);
        }if(resultArray[resultArray.length-1] == score2){
            System.out.println(people[1] + ": " + dog[1] + ", " + score2);
        } if(resultArray[resultArray.length-1] == score3){
            System.out.println(people[2] + ": " + dog[2] + ", " + score3);
        }if(resultArray[resultArray.length-1] == score4){
            System.out.println(people[3] + ": " + dog[3] + ", " + score4);
        }
        if(resultArray[resultArray.length-2] == score1){
            System.out.println(people[0] + ": " + dog[0] + ", " + score1);
        }if(resultArray[resultArray.length-2] == score2){
            System.out.println(people[1] + ": " + dog[1] + ", " + score2);
        } if(resultArray[resultArray.length-2] == score3){
            System.out.println(people[2] + ": " + dog[2] + ", " + score3);
        }if(resultArray[resultArray.length-2] == score4){
            System.out.println(people[3] + ": " + dog[3] + ", " + score4);
        }
        if(resultArray[resultArray.length-3] == score1){
            System.out.println(people[0] + ": " + dog[0] + ", " + score1);
        }if(resultArray[resultArray.length-3] == score2){
            System.out.println(people[1] + ": " + dog[1] + ", " + score2);
        } if(resultArray[resultArray.length-3] == score3){
            System.out.println(people[2] + ": " + dog[2] + ", " + score3);
        }if(resultArray[resultArray.length-3] == score4){
            System.out.println(people[3] + ": " + dog[3] + ", " + score4);
        }





    }
}
