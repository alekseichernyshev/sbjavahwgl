package HW5;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        int[][] array = new int[n][n];
        for (int i = 0; i<n;i++){
            for (int j = 0; j<n; j++){
                array[i][j] = console.nextInt();
            }
        }
        boolean flag = true;
        for (int i = 0; i<n; ++i){
            for (int j = 0; j<n; ++j){
                if (array[i][j] != array[n-j-1][n-i-1]){
                    flag = false;
                    break;
                }
            }
        }
        System.out.println(flag);
    }
}
