package HW5;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();

        Recoursion(n);
    }
    public static void Recoursion(int n){

        if(n<10){
            System.out.print(n+" ");
        }
        else {
            Recoursion(n/10);
            System.out.print(n%10 + " ");
        }

    }
}
