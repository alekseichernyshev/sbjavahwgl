package HW5;

import java.util.Scanner;

public class Task4WithMethods {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        int[][] arr = CreateArray(n);
        int p = console.nextInt();
        int[][] result = DeliteRowsColumn(arr, p, n);
        PrinArray(result);

    }

    public static int[][] CreateArray(int n) {
        int[][] array = new int[n][n];
        Scanner input = new Scanner(System.in);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array[i][j] = input.nextInt();
            }
        }
        return array;
    }

    public static void PrinArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (j == array.length - 1) {
                    System.out.print(array[i][j]);
                } else {
                    System.out.print(array[i][j] + " ");
                }
            }
            System.out.println();
        }
    }

    public static int[][] DeliteRowsColumn(int[][] array, int p, int n) {
        int x = 0;
        int y = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] == p) {
                    x = i;
                    y = j;
                }
            }
        }
        int[][] resultArray = new int[array.length - 1][array.length - 1];
        int ind1, ind2;
        int i, j;
        for (i = 0, ind1 = 0; i<n; i++, ind1++) {
            if (i == x ) {
                ind1--;
                continue;
            }
                for (j = 0, ind2 = 0; j<array[i].length; j++, ind2++) {

                    if (j == y) {
                        ind2--;
                        continue;
                    }
                    resultArray[ind1][ind2] = array[i][j];
                }
        }


        return resultArray;
    }
}
