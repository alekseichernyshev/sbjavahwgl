package HW8.DogShow;

import java.util.ArrayList;

public class Jury {

    private ArrayList<ArrayList<Integer>> scores;
    private int numberOfJuryMembers;

    public ArrayList<ArrayList<Integer>> getScores() {
        return scores;
    }

    public int getNumberOfJuryMembers() {
        return numberOfJuryMembers;
    }

    public void setScores(ArrayList<ArrayList<Integer>> scores) {
        this.scores = scores;
    }

    public void setNumberOfJuryMembers(int numberOfJuryMembers) {
        this.numberOfJuryMembers = numberOfJuryMembers;
    }


}
