package HW8.DogShow;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Jury jury = new Jury();
        jury.setNumberOfJuryMembers(3);
        System.out.println("Пожалуйста, введите количество участников");
        Scanner input = new Scanner(System.in);
        int numberOfParticipant = input.nextInt();
        ArrayList<Participant> participants = Helper.fillParticipants(numberOfParticipant);
        ArrayList<Dog> dogs = Helper.fillDogs(numberOfParticipant);
        ArrayList<ArrayList<Integer>> scores = Helper.fillScores(numberOfParticipant, jury.getNumberOfJuryMembers());
        System.out.println(participants.toString());
        System.out.println(dogs.toString());
        scores.forEach(System.out::println);
        ArrayList<Double> averageScores = Helper.getAverageScores(scores, jury.getNumberOfJuryMembers());
        Competition competition = new Competition();

        ArrayList<Competition> competitors = competition.fillCompetitors(participants, dogs, averageScores);
        competition.sortByAverage(competitors);
        competition.printWinners(competitors);

    }
}
