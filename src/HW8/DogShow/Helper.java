package HW8.DogShow;

import java.util.ArrayList;
import java.util.Scanner;

public class Helper {
    private Helper(){}

    public static ArrayList<Participant> fillParticipants(int n) {
        Scanner input = new Scanner(System.in);
        ArrayList<Participant> participants = new ArrayList<>();

        for (int i = 0; i<n; i++){
            participants.add(new Participant(input.nextLine()));
        }
        return participants;
    }

    public static ArrayList<Dog> fillDogs(int n) {
        Scanner input = new Scanner(System.in);
        ArrayList<Dog> dogs = new ArrayList<>();

        for (int i = 0; i<n; i++){
            dogs.add(new Dog(input.nextLine()));
        }
        return dogs;
    }

    public static ArrayList<ArrayList<Integer>> fillScores(int n, int numbersOfJuryMembers) {
        Scanner input = new Scanner(System.in);
        ArrayList<ArrayList<Integer>> allScores = new ArrayList<>();

        for (int i = 0; i< n; i++){
            ArrayList<Integer> scores = new ArrayList<>();
            for (int j = 0; j<numbersOfJuryMembers; j++){
                scores.add(input.nextInt());
            }
            allScores.add(scores);
        }
        return allScores;
    }

    public static ArrayList<Double> getAverageScores (ArrayList<ArrayList<Integer>> allScores, int numbersOfJuryMembers) {

        ArrayList<Integer> sum = new ArrayList<>();
        for (int i = 0; i< allScores.size(); i++){
            sum.add(allScores.get(i).stream().mapToInt(Integer::valueOf).sum());
        }

        ArrayList<Double> averageScores = new ArrayList<>();
        for (int i = 0; i< sum.size(); i++){
            averageScores.add(Math.floor((sum.get(i) / (double)numbersOfJuryMembers)*Math.pow(10,1))/Math.pow(10,1));
        }
        return averageScores;
    }
}
