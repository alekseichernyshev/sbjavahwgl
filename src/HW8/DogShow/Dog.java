package HW8.DogShow;

public class Dog {
    private String dogsName;

    public Dog(String dogsName) {
        this.dogsName = dogsName;
    }

    public String getDogsName() {
        return dogsName;
    }

    public void setDogsName(String dogsName) {
        this.dogsName = dogsName;
    }

    @Override
    public String toString() {
        return dogsName;
    }
}
