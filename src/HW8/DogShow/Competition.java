package HW8.DogShow;

import HW6.Task2Students.Student;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class Competition  {
    private Participant participant;
    private Dog dog;
    private Double averageScore;

    public Competition(Participant participant, Dog dog, Double averageScore) {
        this.participant = participant;
        this.dog = dog;
        this.averageScore = averageScore;
    }


    public Competition() {
    }


    public Participant getParticipant() {
        return participant;
    }

    public Dog getDog() {
        return dog;
    }

    public Double getAverageScore() {
        return averageScore;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public void setDog(Dog dog) {
        this.dog = dog;
    }

    public void setAverageScore(Double averageScore) {
        this.averageScore = averageScore;
    }

    public  ArrayList<Competition> fillCompetitors(ArrayList<Participant> participants,
                                                  ArrayList<Dog> dogs,
                                                  ArrayList<Double> averageScores) {
        ArrayList<Competition> competitors = new ArrayList<>();
        for (int i = 0; i < dogs.size(); i++) {
            competitors.add(new Competition(participants.get(i), dogs.get(i), averageScores.get(i)));
        }

        return competitors;
    }

    public  void sortByAverage(ArrayList<Competition> competitions) {
        Collections.sort(competitions, new Comparator<Competition>() {
            @Override
            public int compare(Competition o1, Competition o2) {
                return Double.compare(o1.getAverageScore(), o2.getAverageScore());
            }
        });
        Collections.reverse(competitions);
    }

    public  void printWinners(ArrayList<Competition> competitions){
        for (int i = 0; i<3; i++){
            System.out.println(competitions.get(i).toString());
        }
    }

    @Override
    public String toString() {
        return
                "Имя владельца собаки: " + participant +
                ", Кличка собаки: " + dog +
                ", средний балл" + averageScore;
    }
}
