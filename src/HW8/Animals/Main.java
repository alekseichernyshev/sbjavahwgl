package HW8.Animals;

public class Main {
    public static void main(String[] args) {
        Dolphin dolphin = new Dolphin();
        dolphin.swim();
        dolphin.eat();
        dolphin.sleep();
        dolphin.wayOfBirth();
        Bat bat = new Bat();
        bat.fly();
        bat.eat();
        bat.sleep();
        bat.wayOfBirth();
        Eagle eagle = new Eagle();
        eagle.fly();
        eagle.eat();
        eagle.sleep();
        eagle.wayOfBirth();
        GoldFish goldFish = new GoldFish();
        goldFish.swim();
        goldFish.eat();
        goldFish.sleep();
        goldFish.wayOfBirth();

    }
}
