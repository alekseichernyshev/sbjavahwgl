package HW8.BestCarpenterEver;

public class Main {
    public static void main(String[] args) {
        MetalStool stool = new MetalStool("Green");
        Table table = new Table("Red", "Wood");
        MetalStool stool2 = new MetalStool("Silver");
        WoodStool stool3 = new WoodStool("Black");
        Armchair armchair = new Armchair("Brown");

        System.out.println(BestCarpenterEver.check(stool));
        System.out.println(BestCarpenterEver.check(stool2));
        System.out.println(BestCarpenterEver.check(table));
        System.out.println(BestCarpenterEver.check(stool3));
        System.out.println(BestCarpenterEver.check(armchair));
    }
}
