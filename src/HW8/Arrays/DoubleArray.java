package HW8.Arrays;

import java.util.*;
// 3.	На вход передается N — количество столбцов в двумерном массиве и M —
//количество строк. Необходимо вывести матрицу на экран, каждый элемент
//которого состоит из суммы индекса столбца и строки этого же элемента. Решить необходимо используя ArrayList.
public class DoubleArray {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        int m = console.nextInt();

        ArrayList<ArrayList<Integer>> arrayCommon = new ArrayList<>();
        for (int i = 0; i<m; i++){
            ArrayList<Integer> array = new ArrayList<>();
            for (int j = 0; j<n; j++){
                array.add(i+j);
            }
            arrayCommon.add(array);
        }
        arrayCommon.forEach(System.out::println);
    }
}
