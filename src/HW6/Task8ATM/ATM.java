package HW6.Task8ATM;
/*
8.	Реализовать класс “банкомат” Atm.
Класс должен:
●	Содержать конструктор, позволяющий задать курс валют перевода долларов в рубли и курс валют перевода
рублей в доллары (можно выбрать и задать любые положительные значения)
●	Содержать два публичных метода, которые позволяют переводить переданную сумму рублей в доллары и долларов в рубли
●	Хранить приватную переменную счетчик — количество созданных инстансов класса Atm и публичный метод, возвращающий
этот счетчик (подсказка: реализуется через static)

 */
public class ATM {
    private double exchangeRate;
    static int atmCount;

    public ATM(double exchangeRate) {
        this.exchangeRate = exchangeRate;
        ATM.atmCount++;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public static int getAtmCount() {
        return atmCount;
    }

    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public void exchangeRoubles(ATM atm, double roubles){
        double exchangeResult = roubles / atm.getExchangeRate();
        String result = String.format("%.2f", exchangeResult);
        System.out.print(roubles + " руб. равно: " + result + " $.");
    }

    public void exchangeDollars(ATM atm, double dollars){
        double exchangeResult = dollars * atm.getExchangeRate();
        String result = String.format("%.2f", exchangeResult);
        System.out.print(dollars + " $ равно: " + result + " руб.");
    }
    public void atmCount(ATM atm){
        System.out.println("Количество инстансов равно: " + getAtmCount());
    }
}
