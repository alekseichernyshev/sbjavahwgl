package HW6.Task5DayOfWeek;
/*
5.	Необходимо реализовать класс DayOfWeek для хранения порядкового номера дня недели (byte)
и названия дня недели (String).
Затем в отдельном классе в методе main создать массив объектов DayOfWeek длины 7. Заполнить его соответствующими
 значениями (от 1 Monday до 7 Sunday) и вывести значения массива объектов DayOfWeek на экран.
Пример вывода:
1 Monday
2 Tuesday
…
7 Sunday

 */
public class DayOfWeek {
    private byte number;
    private String day;

    public byte getNumber() {
        return number;
    }

    public String getDay() {
        return day;
    }

    public void setNumber(byte number) {
        this.number = number;
    }

    public void setDay(String day) {
        this.day = day;
    }
    public DayOfWeek(byte number, String day){
        this.number = number;
        this.day = day;
    }

    public DayOfWeek() {
    }
}
