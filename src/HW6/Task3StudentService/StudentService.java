package HW6.Task3StudentService;

import HW6.Task2Students.Student;

import java.util.Arrays;
import java.util.Comparator;
/*
3.	Необходимо реализовать класс StudentService.
У класса должны быть реализованы следующие публичные методы:
●	bestStudent() — принимает массив студентов (класс Student из предыдущего задания), возвращает лучшего студента
(т.е. который имеет самый высокий средний балл). Если таких несколько — вывести любого.
●	sortBySurname() — принимает массив студентов (класс Student из предыдущего задания) и сортирует его по фамилии.

 */

 public class StudentService  implements Comparable {

     public static Student bestStudent(Student[] students){
         double[] averageScores = new double[students.length];

         for (int i = 0; i<students.length; i++){
             averageScores[i] = students[i].averageGrades();
         }
         int index = 0;
         double max = averageScores[0];
         for (int i =1; i<averageScores.length; i++){
             if (averageScores[i]>max){
                 max = averageScores[i];
                 index++;
             }
         }
         return students[index];
     }

     public static Student[] sortBySurname(Student[] students){
       Arrays.sort(students, new Comparator<Student>() {
           @Override
           public int compare(Student o1, Student o2) {
               return o1.getSurname().compareTo(o2.getSurname());
           }
       });

        return students;
    }

     @Override
     public int compareTo(Object o) {
         return 0;
     }



}
