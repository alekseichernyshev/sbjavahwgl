package HW6.Task3StudentService;

import HW6.Task2Students.Student;

import java.util.Arrays;

public class Print {
    public static void printArray(Student[] students){
        for (int i = 0; i< students.length; i++){
            System.out.print(students[i].getName() + " " + students[i].getSurname());
            System.out.println(Arrays.toString(students[i].getGrades()));
        }
    }
    public static void printStudent(Student student){
        System.out.print(student.getName() + " " + student.getSurname());
        System.out.println(Arrays.toString(student.getGrades()));
    }
}
