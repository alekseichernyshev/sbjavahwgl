package HW6.Task6AmazingString;

/*
6.	Необходимо реализовать класс AmazingString, который хранит внутри себя строку как массив char и предоставляет следующий функционал:
Конструкторы:
●	Создание AmazingString, принимая на вход массив char
●	Создание AmazingString, принимая на вход String
Публичные методы (названия методов, входные и выходные параметры продумать самостоятельно). Все методы ниже нужно реализовать “руками”, т.е. не прибегая к переводу массива char в String и без использования стандартных методов класса String.
●	Вернуть i-ый символ строки
●	Вернуть длину строки
●	Вывести строку на экран
●	Проверить, есть ли переданная подстрока в AmazingString (на вход подается массив char). Вернуть true, если найдена и false иначе
●	Проверить, есть ли переданная подстрока в AmazingString (на вход подается String). Вернуть true, если найдена и false иначе
●	Удалить из строки AmazingString ведущие пробельные символы, если они есть
●	Развернуть строку (первый символ должен стать последним, а последний первым и т.д.)

 */

public class AmazingString {
    private char[] chars = {' ', ' '};
    private String string;

    public char[] getChars() {
        return chars;
    }

    public String getString() {
        return string;
    }

    public void setChars(char[] chars) {
        this.chars = chars;
    }

    public void setString(String string) {
        this.string = string;
    }

    public AmazingString(char[] chars) {
        this.chars = chars;
    }

    public AmazingString(String string) {
        this.string = string;
    }

    public void returnChar(int n, char[] chars){
        char symbol = chars[n];

        System.out.println(symbol);
    }

    public void getLength(char[] chars){
        int length = chars.length;
        System.out.println(length);
    }
    public void printString(char[] chars){
        for (int i = 0; i<chars.length; i++){
            System.out.print(chars[i]);
        }
    }

    public void containsOrNot (char[] chars1, char[] chars){
        char[] result = new char[chars1.length];

        int index = 0;
        for (int i = 0; i<chars.length; i++){
            if (chars1[0] == chars[i]){
                index = i;
            }
        }

        for (int i = 0; i< result.length; i++){
            result[i] = chars[index];
            index++;
        }

        boolean flag = false;
        for (int i = 0; i< result.length; i++){
            if (result[i] == chars1[i]){
                flag = true;
            }else {
                flag = false;
                break;
            }
        }
        System.out.println(flag);
    }

    public void containsOrNotWithString(String str, char[] chars){

        char[] result = new char[str.length()];

        int index = 0;
        for (int i = 0; i<chars.length; i++){
            if (str.charAt(0) == chars[i]){
                index = i;
                break;
            }
        }

        for (int i = 0; i< result.length; i++){
            result[i] = chars[index];
            index++;
        }

        boolean flag = false;
        for (int i = 0; i< result.length; i++){
            if (result[i] == str.charAt(i)){
                flag = true;
            }else {
                flag = false;
                break;
            }
        }
        System.out.println(flag);

    }

    public void removeSpaces(String str){
        char[] result = new char[string.length()];

        for (int i = 0; i< result.length; i++){
            result[i] = str.charAt(i);
        }
        for (int i = 0; i< result.length; i++){
            if (result[i] != ' '){
                System.out.print(result[i]);
            }
        }
    }

    public void reverseString(String str){
        char[] result = new char[str.length()];
        for (int i = 0; i< result.length; i++){
            result[str.length()-i-1] = str.charAt(i);
        }
        for (int i = 0; i< result.length; i++){
            System.out.print(result[i]);
        }
    }
}
