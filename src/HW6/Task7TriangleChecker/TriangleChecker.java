package HW6.Task7TriangleChecker;
/*
7.	Реализовать класс TriangleChecker, статический метод которого принимает три длины сторон треугольника
и возвращает true, если возможно составить из них треугольник, иначе false.
Входные длины сторон треугольника — числа типа double. Придумать и написать в методе main несколько тестов
 для проверки работоспособности класса (минимум один тест на результат true и один на результат false)
 */
public class TriangleChecker {
    private TriangleChecker(){}




    public static boolean triangleChecker(double firstSide, double secondSide, double thirdSide){
       boolean flag = false;
       if (firstSide < secondSide + thirdSide
               && secondSide < firstSide + thirdSide
               && thirdSide < firstSide + secondSide){
           flag = true;
       }
       return flag;
    }

}
