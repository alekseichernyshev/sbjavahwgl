package HW6;

import HW6.Task2Students.Student;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
         //Task 1
//
//        Cat cat = new Cat();
//        cat.status();
//        cat.status2();

    // Task 2
        Student student = new Student("Иван", "Иванов", new int[]{4, 2, 7, 4, 4, 2, 5, 5, 4, 5});
        System.out.print(student.getName() + " " + student.getSurname());
        System.out.println(Arrays.toString(student.getGrades()));
        System.out.println(student.averageGrades());
        student.newGrade(12);
        System.out.println(Arrays.toString(student.getGrades()));
        System.out.println(student.averageGrades());
        Student student2 = new Student();
        student2.setName("Jack");
        student2.setSurname("Daniels");
        student2.setGrades(new int[]{2, 5, 5, 5, 5, 4, 5, 5, 5, 5});
        System.out.print(student2.getName() + " " + student2.getSurname());
        System.out.println(Arrays.toString(student2.getGrades()));
        System.out.println(student2.averageGrades());
        student2.newGrade(12);
        System.out.println(Arrays.toString(student2.getGrades()));
        System.out.println(student2.averageGrades());








        //Task3
//        Student student1 = new Student("Иван", "Иванов", new int[]{5, 4, 5, 3, 5, 5, 4, 5, 4, 3});
//        Student student2 = new Student("Яков", "Яковлев", new int[]{3, 4, 5, 5, 5, 4, 4, 2, 4, 3});
//        Student student3 = new Student("Фёдор", "Фёдоров", new int[]{2, 2, 4, 3, 2, 5, 3, 5, 4, 3});
//        Student student4 = new Student("Александр", "Александров", new int[]{4, 4, 3, 4, 5, 4, 4, 3, 4, 3});
//        Student student5 = new Student("Юрий", "Юрьев", new int[]{3, 4, 4, 5, 2, 5, 3, 4, 4, 3});
//        Student[] students = {student1, student2, student3, student4, student5};
//        Print.printStudent(StudentService.bestStudent(students));
//        System.out.println();
//        Print.printArray(StudentService.sortBySurname(students));

        // Task4
//       TimeUnit timeUnitAll = new TimeUnit(5, 7, 5);
//        System.out.println("Заданное время");
//        timeUnitAll.printTime(timeUnitAll);
//        timeUnitAll.addHourMinSec(timeUnitAll, 12,35, 77);
//        System.out.println("Время после того как добавили: ");
//        timeUnitAll.printTime(timeUnitAll);
//        System.out.println("Время в двеннадцатичасовом формате: ");
//        timeUnitAll.printTimeTwelveFormat(timeUnitAll);
//        System.out.println("Конструктор только с часами: ");
//        TimeUnit timeUnitHour = new TimeUnit(5);
//        timeUnitHour.printTime(timeUnitHour);
//        System.out.println("Конструктор только с часами двеннадцатичасовом формате: ");
//        timeUnitHour.printTimeTwelveFormat(timeUnitHour);
//        System.out.println("Конструктор с часами и минутами: ");
//        TimeUnit timeUnitHourMinutes = new TimeUnit(15, 8);
//        timeUnitHourMinutes.printTime(timeUnitHourMinutes);
//        System.out.println("Конструктор с часами и минутами двеннадцатичасовом формате: ");
//        timeUnitHourMinutes.printTimeTwelveFormat(timeUnitHourMinutes);

     //Task5
//        DayOfWeek monday = new DayOfWeek((byte) 1, "Monday");
//        DayOfWeek tuesday = new DayOfWeek((byte) 2, "Tuesday");
//        DayOfWeek wednesday = new DayOfWeek((byte) 3, "Wednesday");
//        DayOfWeek thursday = new DayOfWeek((byte) 4, "Thursday");
//        DayOfWeek friday = new DayOfWeek((byte) 5, "Friday");
//        DayOfWeek saturday = new DayOfWeek((byte) 6, "Saturday");
//        DayOfWeek sunday = new DayOfWeek((byte) 7, "Sunday");
//        DayOfWeek[] days = {monday, tuesday, wednesday, thursday, friday, saturday, sunday};
//        for (int i = 0; i<days.length; i++){
//            System.out.println(days[i].getNumber() + " " + days[i].getDay());
//        }

        //Task6
//        AmazingString string = new AmazingString(new char[] {'W', 'e', 'l', 'c', 'o', 'm', 'e', ' ', 't', 'o', ' ', 'j', 'a', 'v', 'a','!'});
//        string.returnChar(3, string.getChars());
//        string.getLength(string.getChars());
//        string.printString(string.getChars());
//        System.out.println();
//        string.containsOrNot(new char[]{'c', 'o', 'm'}, string.getChars());
//        string.containsOrNot(new char[]{'c', 'o', 'j'}, string.getChars());
//        string.containsOrNotWithString("elcom", string.getChars());
//        string.containsOrNotWithString("elca", string.getChars());
//        AmazingString string2 = new AmazingString("Welcome to java!");
//        string2.removeSpaces(string2.getString());
//        System.out.println();
//        string2.reverseString(string2.getString());

        //Task7

//        System.out.println(TriangleChecker.triangleChecker(4.2, 3, 2));
//        System.out.println(TriangleChecker.triangleChecker(6, 220, 12.74));
//        System.out.println(TriangleChecker.triangleChecker(17.21, 12.29, 16.01));
//        System.out.println(TriangleChecker.triangleChecker(21.12, 12.31,7.1));


        //Task8
//        ATM atm1 = new ATM(69.70);
//        atm1.exchangeRoubles(atm1,50.7);
//        System.out.println();
//        atm1.exchangeDollars(atm1, 12);
//        System.out.println();
//        atm1.atmCount(atm1);
//        System.out.println();
//        ATM atm = new ATM(71.22);
//        atm1.atmCount(atm1);

















    }
}
