package HW6.Task2Students;

import java.util.Objects;
import java.util.Random;
import java.util.Scanner;


// 2.	Необходимо реализовать класс Student.
//У класса должны быть следующие приватные поля:
//●	String name — имя студента
//●	String surname — фамилия студента
//●	int[] grades — последние 10 оценок студента. Их может быть меньше, но не может быть больше 10.
//И следующие публичные методы:
//●	геттер/сеттер для name
//●	геттер/сеттер для surname
//●	геттер/сеттер для grades
//●	метод, добавляющий новую оценку в grades. Самая первая оценка должна быть удалена, новая должна сохраниться в конце массива (т.е. массив должен сдвинуться на 1 влево).
//●	метод, возвращающий средний балл студента (рассчитывается как среднее арифметическое от всех оценок в массиве grades)
public class Student  {
    private String name;
    private String surname;
    private int[] grades;

    public Student(){

    }
    public Student(String name, String surname, int[] grades){
        this.name = name;
        this.surname = surname;
        if (grades.length <=10) {
            this.grades = grades;
        }else
            throw new IllegalArgumentException("Оценок не может быть больше десяти");
    }


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGrades(int[] grades) {
        if (grades.length <=10) {
            this.grades = grades;
        }else
            throw new IllegalArgumentException("Оценок не может быть больше десяти");
    }

    public int[] newGrade(int grade) {


        for (int i = 0; i < grades.length-1; i++) {
            grades[i] = grades[i + 1];

        }
        grades[grades.length - 1] = grade;
        return grades;
    }

    public double averageGrades() {

        double sum = 0;
        double average = 0;
        for (int i = 0; i < grades.length; i++) {
            sum += grades[i];
        }
        average = sum / grades.length;

        return average;
    }


}
