package HW6.Task4TimeUnit;
/*
4.	Необходимо реализовать класс TimeUnit с функционалом, описанным ниже (необходимые поля продумать самостоятельно). Обязательно должны быть реализованы валидации на входные параметры.
Конструкторы:
●	Возможность создать TimeUnit, задав часы, минуты и секунды.
●	Возможность создать TimeUnit, задав часы и минуты. Секунды тогда должны проставиться нулевыми.
●	Возможность создать TimeUnit, задав часы. Минуты и секунды тогда должны проставиться нулевыми.
Публичные методы:
●	Вывести на экран установленное в классе время в формате hh:mm:ss
●	Вывести на экран установленное в классе время в 12-часовом формате (используя hh:mm:ss am/pm)
●	Метод, который прибавляет переданное время к установленному в TimeUnit (на вход передаются только часы, минуты и секунды).

 */
public class TimeUnit {
    private int hours;
    private int minutes;
    private int seconds;

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setHours(int hours) {
        if(hours>0 && hours<24){
            this.hours = hours;
        }
        else
            throw new IllegalArgumentException("Не известное значение");
    }

    public void setMinutes(int minutes) {
        if(minutes>0 && minutes<60){
            this.minutes = minutes;
        }else
            throw new IllegalArgumentException("Не известное значение");
    }

    public void setSeconds(int seconds) {
        if(seconds>0 && seconds<60){
            this.seconds = seconds;
        }
        else
            throw new IllegalArgumentException("Не известное значение");
    }


    public TimeUnit(int hours, int minutes){
        if (hours>0 && hours<24 && minutes>0 && minutes<60) {
            this.hours = hours;
            this.minutes = minutes;
            seconds = 00;
        }else
            throw new IllegalArgumentException("Не известное значение");
    }
    public TimeUnit(int hours){
        if (hours>0&&hours<24) {
            this.hours = hours;
            minutes = 00;
            seconds = 00;
        }else
            throw new IllegalArgumentException("Не известное значение");
    }
    public TimeUnit(int hours, int minutes, int seconds){
        if (hours>0 && hours<24 && minutes>0 &&minutes<60 && seconds>0 &&seconds<60) {
            this.hours = hours;
            this.minutes = minutes;
            this.seconds = seconds;
        }else
            throw new IllegalArgumentException("Не известное значение");
    }

    public TimeUnit() {
    }
    public void printTime(TimeUnit timeUnit){
        String h = Integer.toString(timeUnit.getHours());
        String m = Integer.toString(timeUnit.getMinutes());
        String s = Integer.toString(timeUnit.getSeconds());
        if (h.length()<2){
            h = "0" + h;
        }
        if (m.length()<2){
            m = "0" + m;
        }
        if (s.length()<2){
            s = "0" + s;
        }
        System.out.println(h + ":" + m + ":" + s);
    }

    public void printTimeTwelveFormat(TimeUnit timeUnit){
        String m = Integer.toString(timeUnit.getMinutes());
        String s = Integer.toString(timeUnit.getSeconds());
        boolean flag = false;
        String format = "";
        String h = "";
        int h1 = timeUnit.getHours();
        if (h1>12){
            h1 -=12;
            flag = true;
        }
        h = Integer.toString(h1);
        if (!flag){
            format = "am";
        }else
            format = "pm";

        if (h.length()<2){
            h = "0" + h;
        }
        if (m.length()<2){
            m = "0" + m;
        }
        if (s.length()<2){
            s = "0" + s;
        }
        System.out.println(h + ":" + m + ":" + s + " " + format);
    }

    public  void addHourMinSec(TimeUnit timeUnit, int addhours, int addminutes, int addseconds){

        int h = timeUnit.getHours();
        int m = timeUnit.getMinutes();
        int s = timeUnit.getSeconds();
        addhours += h;
        addminutes += m;
        addseconds += s;
        while (addseconds>59){
            addseconds -=60;
            addminutes++;
        }

        while (addminutes>59){
            addminutes -=60;
            addhours++;
        }

        while (addhours>23){
            addhours -= 24;
        }
        int newHour = addhours;
        int newMinutes = addminutes;
        int newSecond = addseconds;

        timeUnit.setHours(newHour);
        timeUnit.setMinutes(newMinutes);
        timeUnit.setSeconds(newSecond);


    }
}
