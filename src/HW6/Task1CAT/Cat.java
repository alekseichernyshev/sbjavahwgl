package HW6.Task1CAT;

import java.util.Random;
// 1.	Необходимо реализовать класс Cat.
//У класса должны быть реализованы следующие приватные методы:
//●	sleep() — выводит на экран “Sleep”
//●	meow() — выводит на экран “Meow”
//●	eat() — выводит на экран “Eat”
//И публичный метод:
//status() — вызывает один из приватных методов случайным образом.

    public class Cat {
        private void sleep(){
            System.out.println("Sleep");
        }
        private void meow(){
            System.out.println("Meow");
        }
        private void eat(){
            System.out.println("Eat");
        }

        public void status(){
            Random random = new Random();
            int n = random.nextInt(3);
            if(n == 0){
                sleep();
            }if (n == 1){
                meow();
            }if(n == 2) {
                eat();
            }
        }

        public void status2(){
            switch ((int) (Math.random()*3)){
                case 0-> sleep();
                case 1 -> meow();
                case 2 -> eat();
            }
        }
    }


